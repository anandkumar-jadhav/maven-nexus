FROM glassfish:4.1-jdk8
ADD  target/sparkjava-hello-world-1.0.war /root
ADD start.sh /root
RUN chmod u+x /root/start.sh
EXPOSE 4848
EXPOSE 8080
ENTRYPOINT ["/root/start.sh"]