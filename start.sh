#!/bin/bash
PID_FILE=$GLASSFISH_HOME/glassfish/domains/domain1/config/pid
asadmin start-domain
asadmin deploy --contextroot / --name current-app /root/sparkjava-hello-world-1.0.war
inotifywait -qq -e delete_self $PID_FILE